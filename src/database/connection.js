const mysql = require('mysql2/promise');

module.exports = async function databaseConnection() {
  try {

    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'cidenet',
      connectionLimit: 100
    });

    return connection;

  } catch (error) {
    console.log(`
      ❌ Failed database connection:
      ${error}
    `);
  }
}