const { Router } = require('express');
const EmployeeController = require('./../controllers/employee.controller');

const router = Router();

router.post('/', EmployeeController.create);

module.exports = router;
