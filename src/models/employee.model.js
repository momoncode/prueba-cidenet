const databaseConnection = require('./../database/connection');

const EmployeeModel = {

  async create(identification, first_name, other_name, first_lastname, second_lastname, country, identification_type, email, admision_date, area) {

    try {
      const database = await databaseConnection();

      const query = "INSERT INTO employees (identification,first_name,other_name,first_lastname,second_lastname,country,identification_type,email,admision_date,area) VALUES (?,?,?,?,?,?,?,?,?,?)";
      await database.execute(query, [identification, first_name, other_name, first_lastname, second_lastname, country, identification_type, email, admision_date, area]);

      await database.end();
      return true;

    } catch (error) {

      console.log(`
        ❌ Failed to save employee.
        ${error}
      `);

      return false;
    }
  },

  async isUsingEmail(email) {

    try {
      const database = await databaseConnection();

      const query = "SELECT email FROM employees WHERE email = ?";
      const [rows] = await database.execute(query, [email]);

      console.log(rows);

      await database.end();

      return rows.length > 0;

    } catch (error) {

      console.log(`
        ❌ Failed to find email employee.
        ${error}
      `);

      return false;
    }
  },

  async getConsecutive(first_name, first_lastname, country) {

    try {
      const database = await databaseConnection();

      const query = "SELECT COUNT(email) AS consecutive FROM employees WHERE first_name = ? AND first_lastname = ? AND country = ?";
      const [rows] = await database.execute(query, [first_name, first_lastname, country]);

      const { consecutive } = rows[0];

      await database.end();

      return consecutive;

    } catch (error) {

      console.log(`
        ❌ Failed to find consecutive email employee.
        ${error}
      `);

      return null;
    }
  },

}

module.exports = EmployeeModel;
