const DataValidator = {
  isValidLength(value, min = 1, max = 300) {
    return value.length >= max && value.length <= min;
  },
  lowerCaseValue(value) {
    return value.toLowerCase();
  }
}

module.exports = DataValidator;
