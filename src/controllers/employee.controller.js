const EmployeeModel = require('./../models/employee.model');
const { isValidLength, lowerCaseValue } = require('./../libs/dataValidatos');

const EmployeeController = {

  async generateEmail(first_name, first_lastname, country) {
    const domain = country == "co" ? "cidenet.com.co" : "cidenet.com.us";

    let email = `${first_name}.${first_lastname}@${domain}`;

    const isUsingEmail = await EmployeeModel.isUsingEmail(email);

    if (!isUsingEmail) return email;

    const consecutive = await EmployeeModel.getConsecutive(first_name, first_lastname, country);

    email = `${first_name}.${first_lastname}.${consecutive}@${domain}`;
    return email;

  },

  async create(req, res) {
    const { identification, first_name, other_name, first_lastname, second_lastname, country, identification_type, admision_date, area } = req.body;

    if (isValidLength(identification, 1, 20)) return res.status(200).json({ message: "La indentificación debe tener menos de 20 caracteres." });
    if (isValidLength(first_name, 1, 20)) return res.status(200).json({ message: "El nombre debe tener menos de 20 caracteres." });
    if (isValidLength(other_name, 1, 50)) return res.status(200).json({ message: "El otro nombres debe tener menos de 50 caracteres." });
    if (isValidLength(first_lastname, 1, 20)) return res.status(200).json({ message: "El primer apellido debe tener menos de 20 caracteres." });
    if (isValidLength(second_lastname, 1, 20)) return res.status(200).json({ message: "El segundo apellido debe tener menos de 20 caracteres." });
    if (isValidLength(first_name, 1, 20)) return res.status(200).json({ message: "El nombre debe tener menos de 20 caracteres." });
    if (isValidLength(first_name, 1, 20)) return res.status(200).json({ message: "El nombre debe tener menos de 20 caracteres." });
    if (isValidLength(first_name, 1, 20)) return res.status(200).json({ message: "El nombre debe tener menos de 20 caracteres." });
    if (isValidLength(first_name, 1, 20)) return res.status(200).json({ message: "El nombre debe tener menos de 20 caracteres." });

    const email = await EmployeeController.generateEmail(first_name, first_lastname, country);

    await EmployeeModel.create(identification, lowerCaseValue(first_name), lowerCaseValue(other_name), lowerCaseValue(first_lastname), lowerCaseValue(second_lastname), country, identification_type, email, admision_date, area);

    res.status(201).json({
      message: "Created!"
    });

  }

}

module.exports = EmployeeController;
