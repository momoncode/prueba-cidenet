const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const employeeRoutes = require('./routes/employee.routes');

const app = express();

app.set('PORT', process.env.PORT || 3100);

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

app.use('/api/employee', employeeRoutes);

app.listen(app.get('PORT'), () => {
  console.log(`
    ✅ App runing on port ${app.get('PORT')}
  `);
});